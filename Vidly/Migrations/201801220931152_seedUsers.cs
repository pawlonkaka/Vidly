namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'003edff8-e5ca-4006-9b83-7fae9e148f34', N'pawlonkakamil@gmail.com', 0, N'AEYYdlcCwcK4Ku+ZHA6T6PHXuA9qmkSUpW+imjs+QfM5IdU2RJavB4I15WctTLVD8Q==', N'08e3b5ad-664f-43b2-ba6c-a98f143df64e', NULL, 0, 0, NULL, 1, 0, N'pawlonkakamil@gmail.com')
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'626fde2b-9e15-43a2-85b4-fc6b83828285', N'guest@vidly.com', 0, N'ADFIqtLQ3XeBZwGqqPKdOSKWHuoeKjWT7LmUI59sQN/4OXjmA1Csv8dsCSuFJ9X6kA==', N'849a72fa-a764-441a-9adf-267486703e91', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c438f890-7984-4f16-8dda-b2149a73fa76', N'admin@vidly.com', 0, N'AEc212XBBOYOjXtRfAIV/W8F+Qxk4ZI7T9NKPb+vRh2tidf1/Hn9AtiJTCQkV3jboQ==', N'fd0697b4-41a0-452c-ace4-d518ea62bc8e', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
            
            INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'4535f981-9594-44b3-8a28-0e7daca75aee', N'CanManageMovie')
            INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c438f890-7984-4f16-8dda-b2149a73fa76', N'4535f981-9594-44b3-8a28-0e7daca75aee')

            ");
        }
        
        public override void Down()
        {
        }
    }
}
