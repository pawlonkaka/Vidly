﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.Dtos
{
    public class CustomerDto
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public bool isSubscribeToNewsletter { get; set; }

        [Required]        
        public byte MembershipTypeId { get; set; }

        public MemberShipTypeDto membershipType { get; set; }

        //[Min18YearsIfAMember]
        public DateTime? Brithdate { get; set; }
    }
}