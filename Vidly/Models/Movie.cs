﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Movie
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name="Title")]
        public string Name { get; set; }


        public Genre genre { get; set; }

        [Required]
        [Display(Name ="Genre")]
        public byte GenreId { get; set; }

        
        [Display(Name ="Release Date")]
        public DateTime ReleaseDate { get; set; }

        
        [Display(Name ="Added Date")]
        public DateTime DateAdded { get; set; }

        
        [Display(Name ="Number in Stock")]
        [Range(0,1000)]
        [DefaultValue(0)]
        public int NumberInStock { get; set; }
    }
}