﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Vidly.Models;
using System.Data.Entity;
using Vidly.ViewModels;
using Vidly.Migrations;

namespace Vidly.Controllers
{
    [Authorize(Roles = RoleName.CanManageMovieAndCustomer)]
    public class CustomerController : Controller
    {
        private ApplicationDbContext _context;

        public CustomerController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult New()
        {

            var membersipTypes = _context.MembershipTypes.ToList();
            var viewModel = new CustomerFormViewModel
            {
                Customer = new Customer(),
                MembershipTypes = membersipTypes
            };

            return View("CustomerForm",viewModel);    
        }
        public ActionResult Edit(int id)
        {

            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            var viewModel = new CustomerFormViewModel
            {
                Customer = customer,
                MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm",viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new CustomerFormViewModel
                {
                    Customer = customer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };

                return View("CustomerForm", viewModel);
            }

            if(customer.Id == 0)
            {
                _context.Customers.Add(customer);
            }
            else
            {
                var customerToEdit = _context.Customers.SingleOrDefault(c => c.Id == customer.Id);

                customerToEdit.Name = customer.Name;
                customerToEdit.Brithdate = customer.Brithdate;
                customerToEdit.MembershipTypeId = customer.MembershipTypeId;
                customerToEdit.isSubscribeToNewsletter = customer.isSubscribeToNewsletter;
            }
            
            _context.SaveChanges();

            return RedirectToAction("Index","Customer");
        }

        public ViewResult Index()
        {
            return View();
        }

       

    }
}