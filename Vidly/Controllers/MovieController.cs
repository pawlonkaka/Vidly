﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;
using System;

namespace Vidly.Controllers
{
    [Authorize]
    public class MovieController : Controller
    {
        private ApplicationDbContext _context;

        public MovieController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        [Authorize(Roles = RoleName.CanManageMovieAndCustomer)]
        public ActionResult New()
        {

            var genreType = _context.Genres.ToList();

            var viewModel = new MovieFormViewModel
            {
                Movie = new Movie(),
                Genres = genreType
            };

            return View("MovieForm",viewModel);
        }

        [Authorize(Roles = RoleName.CanManageMovieAndCustomer)]
        public ActionResult Edit(int id)
        {

            var movie = _context.Movies.SingleOrDefault(m => m.Id == id);

            if (movie == null)
                return HttpNotFound();

            var viewModel = new MovieFormViewModel
            {
                Movie = movie,
                Genres = _context.Genres.ToList()
            };

            return View("MovieForm",viewModel);
        }

        [HttpPost]
        [Authorize(Roles = RoleName.CanManageMovieAndCustomer)]
        [ValidateAntiForgeryToken]
        public ActionResult Update(Movie movie)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new MovieFormViewModel
                {
                    Genres = _context.Genres.ToList()
                };

                return View("MovieForm", viewModel);
            }

            if(movie.Id == 0)
            {
                movie.DateAdded = DateTime.Now;
                _context.Movies.Add(movie);
            }
            else
            {
                var movieToEdit = _context.Movies.Single(m => m.Id == movie.Id);

                movieToEdit.Name = movie.Name;
                movieToEdit.GenreId = movie.GenreId;
                movieToEdit.ReleaseDate = movie.ReleaseDate;
                movieToEdit.NumberInStock = movie.NumberInStock;
                }

            _context.SaveChanges();

            return RedirectToAction("Index","Movie");
        }

        public ViewResult Index()
        {
            if (User.IsInRole(RoleName.CanManageMovieAndCustomer))
                return View("List");

            return View("ReadOnlyList");
        }
    }
}